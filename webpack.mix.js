const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'node_modules/admin-lte/plugins/jquery/jquery.js',
    'node_modules/admin-lte/plugins/jquery-ui/jquery-ui.js',
    'node_modules/admin-lte/plugins/bootstrap/js/bootstrap.bundle.js',
    'node_modules/admin-lte/plugins/chart.js/Chart.js',
    'node_modules/admin-lte/plugins/sparklines/sparkline.js',
    'node_modules/admin-lte/plugins/jqvmap/jquery.vmap.js',
    'node_modules/admin-lte/plugins/jqvmap/maps/jquery.vmap.world.js',
    'node_modules/admin-lte/plugins/jquery-knob/jquery.knob.js',
    'node_modules/admin-lte/plugins/moment/moment.min.js',
    'node_modules/admin-lte/plugins/daterangepicker/daterangepicker.js',
    'node_modules/admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js',
    'node_modules/admin-lte/plugins/summernote/summernote-bs4.js',
    'node_modules/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
    'node_modules/admin-lte/dist/js/adminlte.js'
], 'public/js/app.js');

mix.sass('resources/sass/app.scss', 'public/css');
